package com.franga2000.snek.game;

import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.IOException;

import javax.imageio.ImageIO;

public class Thing {
	public int x;
	public int y;
	
	public Direction direction = Direction.UP;
	Direction oldDirection = null;
	
	BufferedImage originalSprite;
	public BufferedImage sprite;
	
	public Thing(int x, int y, String sprite) {
		this.x = x;
		this.y = y;
		try {
			this.originalSprite = ImageIO.read(getClass().getResource("/" + sprite));
		} catch (IOException e) {
			e.printStackTrace();
		}
		renderSprite();
	}
	
	public void tick() {
		if (oldDirection != direction) {
			renderSprite();
		}
		oldDirection = direction;
	}	
	
	private void renderSprite() {
		sprite = new BufferedImage(originalSprite.getWidth(), originalSprite.getHeight(), originalSprite.getType());
	    Graphics2D g = (Graphics2D) sprite.getGraphics();
		
		g.rotate(Math.toRadians(this.direction.angle), 32, 32);
		g.drawRenderedImage(originalSprite, null);
		
	    g.dispose();
	}

	public boolean sameCoords(Thing other) {
		return 	this.x == other.x &&
				this.y == other.y;
	}
	
	public boolean coordsExist(Game game) {
		return 	this.x > game.w ||
				this.y > game.h;
	}
}
