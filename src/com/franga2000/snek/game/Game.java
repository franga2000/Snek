package com.franga2000.snek.game;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.ListIterator;
import java.util.Map.Entry;

import com.franga2000.util.Util;

public class Game {
	public int w;
	public int h;
	public int scale = 32;
	
	public int tick = 0;
	
	public ArrayList<Entry<String, Direction>> commandQueue = new ArrayList<Entry<String, Direction>>(); 
	
	public ArrayList<Appel> appels = new ArrayList<Appel>();
	
	public HashMap<String, Snek> sneks = new HashMap<String, Snek>();
	
	public Game() {
		// TODO: Custom size
		this.w = 15;
		this.h = 15;
	
		appels.add(new Appel(1, 1));
		appels.add(new Appel(5, 7));
		appels.add(new Appel(0, 0));
		appels.add(new Appel(14, 14));
	}
	
	public void tick() {
		for (Entry<String, Direction> cmd : commandQueue) {
			Snek snek = sneks.get(cmd.getKey());
			if (!cmd.getValue().opposite(snek.direction))
				snek.direction = cmd.getValue();
		}
		
//		int i = 0;
		for (Entry<String, Snek> e : sneks.entrySet()) {		
			Snek snek = e.getValue();
			
			snek.tick();
			
			ListIterator<Appel> iter = appels.listIterator();
			Appel appel;
			while ((appel = iter.hasNext() ? iter.next(): null) != null){
				if (appel.sameCoords(snek)) {
					snek.lengthen();
					iter.remove();
				}
			}
			
//			i++;
		}
		
		while (appels.size() < 2) {
			int x, y;
			do {
				x = Util.randInt(0, w-1);
				y = Util.randInt(0, h-1);
				System.out.println("appel at " + x + "," + y);
			} while (!empty(x, y));
			appels.add(new Appel(x, y));
		}
		
		tick++;
	}

	private boolean empty(int x, int y) {
		for (Snek snek : sneks.values()) 
			if (snek.sittingOn(x, y))
				return false;
		for (Appel appel : appels) 
			if (appel.x == x && appel.y == y)
				return false;
		
		return true;
	}

}
