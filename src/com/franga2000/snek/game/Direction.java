package com.franga2000.snek.game;

/**
 * <p>Represents a direction to move in.<br>
 * The x and y integer fields contain the change in location that the Direction represents.</p>
 * <p>For example: DOWN.x = 0, DOWN.y = -1 (meaning "move 0 on x and -1 on y")</p> 
 * 
 * @author franga2000
 */
public enum Direction {
	/*
	 * TODO: Maybe rename to Vector, as that what this actually is
	 */
	UP(0,1,0),
	DOWN(0,-1,180),
	LEFT(-1,0,-90),
	RIGHT(1,0,90);
	
	public final int x, y, angle;
	
	Direction(int x, int y, int angle) {
		this.x = x;
		this.y = y;
		this.angle = angle; // Too lazy to figure this out in the rotate function, so I'm hard-coding it here. Deal with it!
	}

	public boolean opposite(Direction other) {
		return 	Math.max(this.x, other.x) + Math.min(this.x, other.x) == 0 &&
				Math.max(this.y, other.y) + Math.min(this.y, other.y) == 0;
	}
}
