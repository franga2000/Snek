package com.franga2000.snek.game;

import java.util.LinkedList;

public class SnekPiece extends Thing {
	public LinkedList<Direction> commandQueue = new LinkedList<Direction>();

	public SnekPiece(int x, int y, String sprite) {
		super(x, y, sprite);
	}
	
	public SnekPiece(int x, int y) {
		super(x, y, "snek_body.png");
	}
	
	@Override
	public void tick() {
		this.direction = this.commandQueue.poll();
		super.tick();
		this.x += this.direction.x;
		this.y += this.direction.y;
		
	}

}
