package com.franga2000.snek.game;

import java.util.LinkedList;

public class Snek extends SnekPiece {
	public LinkedList<SnekPiece> body = new LinkedList<SnekPiece>();
	public SnekPiece tail;
	
	public Snek(int x, int y) {
		super(x, y, "snek_head.png");
		
		tail = new SnekPiece(x, y-1, "snek_tail.png");
		tail.commandQueue.add(direction);
	}
	
	 @Override
	public void tick() {
		this.commandQueue.addFirst(this.direction);
		super.tick();
		
		for (SnekPiece piece : body) {
			piece.commandQueue.add(this.direction);
			//piece.direction = piece.commandQueue.poll();
			piece.tick();
		}
		tail.commandQueue.add(this.direction);
		//tail.direction = tail.commandQueue.poll();
		tail.tick();
	}

	public int score() {
		return this.body.size();
	}

	@SuppressWarnings("unchecked")
	public void lengthen() {
		SnekPiece piece = new SnekPiece(tail.x, tail.y);
		piece.commandQueue = (LinkedList<Direction>) tail.commandQueue.clone();
		body.addLast(piece);
		
		tail.x -= tail.direction.x;
		tail.y -= tail.direction.y;
		tail.commandQueue.addFirst(tail.direction);
	}
	
	public boolean sittingOn(int x, int y) {
		if ((this.x == x && this.y == y) || 
			(tail.x == x && tail.y == y))
			return true;
		
		for (SnekPiece piece : body)
			if (piece.x == x && piece.y == y)
				return true;
		return false;
	}
	
}
