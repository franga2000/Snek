package com.franga2000.snek;

import java.awt.Canvas;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.image.BufferStrategy;
import java.util.Map.Entry;

import javax.swing.JFrame;

import com.franga2000.snek.game.Appel;
import com.franga2000.snek.game.Game;
import com.franga2000.snek.game.Snek;
import com.franga2000.snek.game.SnekPiece;
import com.franga2000.snek.game.Thing;

public class Main {
	
	static Game game;

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		game = new Game();
		game.scale *= 1.5;
		
		Snek snek1 = new Snek(5, 2);
		game.sneks.put("Player 1", snek1);
		
		
		JFrame window = new JFrame("SNEK GEM!");
		windowInit(window);
		window.addKeyListener(new SwingKeypressHandler(game, snek1, null));
		
		Canvas canvas = new Canvas();
		canvasInit(canvas);
		
		window.add(canvas);
		canvas.createBufferStrategy(3);
		
		BufferStrategy bufferStrategy;
		Graphics graphics;
		boolean running = true;
		int frame = 0;
		
		while (running) {
			bufferStrategy = canvas.getBufferStrategy();
			// Prep canvas and clear it
			graphics = bufferStrategy.getDrawGraphics();
			graphics.clearRect(0, 0, game.w * game.scale, game.h * game.scale);
			
			// DEBUG: show game tick number
			graphics.setColor(Color.RED);
			graphics.drawString("Tick: " + game.tick, 5, 15);
			graphics.drawString("Frame: " + frame, 5, 30);
			
			// TODO: Use a separate clock (and thread) for the game
			if (frame % 300 == 0 || frame == 0) {
				game.tick();
			}
			
			// Draw apples
			for (Appel appel : game.appels) {
				draw(graphics, appel);
			}
			
			// Draw snakes
			int i = 1;
			for (Entry<String, Snek> e : game.sneks.entrySet()) {		
				Snek snek = e.getValue();
				
				// Draw snake score
				graphics.setColor(Color.GREEN);
				graphics.drawString(e.getKey() + ": " + snek.score(), 5, 30 + 20*i);
				
				// Draw snake
				draw(graphics, snek);
				for (SnekPiece piece : snek.body) {
					draw(graphics, piece);
				}
				draw(graphics, snek.tail);
				
				i++;
			}
			
			// Render the canvas
			bufferStrategy.show();
			graphics.dispose();
			
			frame++;
			
			// Hacky was of doing the timer
			// TODO: Remove after switching game to separate timer
			try {
				Thread.sleep(1);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}
	
	/**
	 * Draw a <code>Thing</code>
	 * @param graphics - The <code>Graphics</code> object to draw the <code>Thing</code> on
	 * @param thing - The <code>Thing</code> to draw on the <code>Graphics</code> object
	 */
	static void draw(Graphics graphics, Thing thing) {
		int cx = thing.x * game.scale;
		int cy = game.h * game.scale - thing.y * game.scale - game.scale;
		graphics.drawImage(thing.sprite, cx, cy, game.scale, game.scale, null);
		graphics.drawRect(cx, cy, game.scale, game.scale);
		// DEBUG: System.out.printf("Drawing: %s (%d, %d) to (%d, %d)\n", thing, thing.x, thing.y, cx, cy);
		graphics.drawString("" + thing.direction, cx, cy);
	}

	private static void canvasInit(Canvas canvas) {
		canvas.setSize(game.w * game.scale, game.h * game.scale + game.scale);
		canvas.setBackground(Color.BLACK);
		canvas.setVisible(true);
		canvas.setFocusable(false);
	}

	private static void windowInit(JFrame window) {
		window.setSize(game.w * game.scale, game.h * game.scale + game.scale);
		window.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		window.setLocationRelativeTo(null);
		window.setResizable(false);
		window.setVisible(true);
	}

}
