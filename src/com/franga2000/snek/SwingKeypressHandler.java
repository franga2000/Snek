package com.franga2000.snek;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import com.franga2000.snek.game.Direction;
import com.franga2000.snek.game.Game;
import com.franga2000.snek.game.Snek;

public class SwingKeypressHandler extends InputHandler implements KeyListener {
	private Snek snek1;
	private Snek snek2;

	public SwingKeypressHandler(Game game, Snek snek1, Snek snek2) {
		super(game);
		this.snek1 = snek1;
		this.snek2 = snek2;
	}

	@Override
	public void keyPressed(KeyEvent evt) {
		Direction dir = null;
		
		if (snek1 != null) {
			switch(evt.getKeyCode()) {
				case KeyEvent.VK_UP: {
					dir = Direction.UP;
				}break;
				
				case KeyEvent.VK_DOWN: {
					dir = Direction.DOWN;		
				}break;
				
				case KeyEvent.VK_LEFT: {
					dir = Direction.LEFT;
				}break;
				
				case KeyEvent.VK_RIGHT: {
					dir = Direction.RIGHT;
				}break;
			}
			if (dir != null && !dir.opposite(snek1.direction))
				snek1.direction = dir;
		}
		
		
		if (snek2 != null) {
			switch(evt.getKeyCode()) {
				case KeyEvent.VK_W: {
					snek2.direction = Direction.UP;
				}break;
				
				case KeyEvent.VK_S: {
					snek2.direction = Direction.DOWN;		
				}break;
				
				case KeyEvent.VK_A: {
					snek2.direction = Direction.LEFT;
				}break;
				
				case KeyEvent.VK_D: {
					snek2.direction = Direction.RIGHT;
				}break;
			}
		if (dir != null && !dir.opposite(snek1.direction))
			snek2.direction = dir;
	}
	}

	@Override
	public void keyReleased(KeyEvent evt) {
		// Don't care
	}

	@Override
	public void keyTyped(KeyEvent evt) {
		// Don't care
	}

}
