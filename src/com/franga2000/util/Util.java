package com.franga2000.util;

import java.util.Random;

/**
 * Copy-pasted utility functions, mostly from StackOverflow
 * @author The Internet :D
 */
public class Util {
	
	static Random rand = new Random();
	/**
	 * Returns a psuedo-random number between min and max, inclusive.
	 * The difference between min and max can be at most
	 * <code>Integer.MAX_VALUE - 1</code>.
	 *
	 * @param min Minimim value
	 * @param max Maximim value.  Must be greater than min.
	 * @return Integer between min and max, inclusive.
	 * @see java.util.Random#nextInt(int)
	 * 
	 * @author <a href="https://stackoverflow.com/users/2310289/scary-wombat">Scary Wombat</a>
	 */
	public static int randInt(int min, int max) {
	    int randomNum = rand.nextInt((max - min) + 1) + min;
	    return randomNum;
	}

}
